// модальное окно с объявлением, можно выпилить
export const state = () => ({
  showModalOffer: false,
  modalOfferLoading: false,
  modalOffer: {}
})

export const mutations = {
  showModalOffer (state, offer) {
    state.showModalOffer = true
    state.modalOffer = offer
  },

  hideModalOffer (state) {
    state.showModalOffer = false
  },

  setModalOfferLoading (state, isLoading) {
    state.modalOfferLoading = isLoading
  }
}

export const actions = {
  async fetchAndShowModalOffer (context, id) {
    context.commit('setModalOfferLoading', true)
    try {
      const { result } = await this.$api('offers.getFast', {
        offerId: id
      })
      context.commit('showModalOffer', result)
    } catch {

    }
    context.commit('setModalOfferLoading', false)
  },

  hideModalOffer (context) {
    context.commit('hideModalOffer')
  }
}
