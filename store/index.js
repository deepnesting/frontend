import Vue from 'vue'
import { v4 as uuidv4 } from 'uuid'

export const state = () => ({
  hideMobileAlert: false,
  socket: {},
  unreadCount: null,
  dark: false,
  hasHelpMessage: false,
  loadId: ''
})

export const mutations = {
  toggleDark: state => (state.dark = !state.dark),

  hideMobileAlert: state => (state.hideMobileAlert = true),

  SOCKET_ONOPEN (state, event) {
    Vue.prototype.$socket = event.currentTarget
    state.socket.isConnected = true
    if (this.$auth.loggedIn) {
      event.currentTarget.send(JSON.stringify({ type: 'auth', data: { token: this.$auth.getToken('local') } }) + '')
    }
    // console.log('open', event.currentTarget)
  },

  SOCKET_ONCLOSE (state) {
    state.socket.isConnected = false
  },

  SOCKET_ONERROR (/* state, event */) {},

  // default handler called for all methods
  SOCKET_ONMESSAGE (state, message) {
    //  console.log(state,message)
    try {
      const msg = JSON.parse(message.data)
      switch (msg.type) {
        case 1: // need ticket moderation
          // let finded = state.moderation.list.find(x => x.id === msg.payload.id)
          // console.log(
          //   "new  moderation ticket",
          //   msg.payload.id,
          //   finded,
          //   state.moderation.list.map(x => x.id)
          // )
          // if (!finded) {
          //   state.moderation.list.push(msg.payload)
          // }
          break
        case 'message':
          // this.$toast.success('Новое сообщение!')
          break
        case 'helpAnswer':
          console.log('help answered', state)
          state.hasHelpMessage = true
          break
        default:
          console.log('incoming ws message', msg)
      }
    } catch {
      //
    }
  },

  // mutations for reconnect methods
  SOCKET_RECONNECT (/* state, count */) {},

  SOCKET_RECONNECT_ERROR (state) {
    state.socket.reconnectError = true
  },

  setUnreadCount (state, count) {
    state.unreadCount = count
  },

  resetHasHelpMessage (state) {
    state.hasHelpMessage = false
  },

  generateLoadId (state) {
    state.loadId = uuidv4()
  }
}

export const actions = {
  sendMessage (_, message) {
    Vue.prototype.$socket.send(message)
  },

  async fetchUnreadCount (context) {
    const { result } = await this.$api('messenger.unreadCount')
    if (result) {
      context.commit('setUnreadCount', result)
    }
  },

  nuxtClientInit ({ commit }) {
    commit('generateLoadId')
  }
}
