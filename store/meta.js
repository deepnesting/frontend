export const state = () => ({
  meta: {
    url: '',
    title: '',
    description: '',
    breadcrumbs: [],
    searchOffers: {
      city: 0,
      searchType: 0,
      buildType: 0,
      roomCount: [],
      area: [],
      price: 0,
      priceFrom: 0,
      priceTo: 0
    }
  }
})

// items: [
//   {
//     value: 1,
//     title: 'Квартиру'
//   },
//   {
//     value: 2,
//     title: 'Комнату'
//   },
//   {
//     value: 3,
//     title: 'Дом'
//   },
//   {
//     value: 4,
//     title: 'Койко-место'
//   }
// ]

export const mutations = {
  setMeta (state, meta) {
    // init area for avoid null error
    if (meta.searchOffers) {
      if (!meta.searchOffers.area) {
        meta.searchOffers.area = []
      }
    }

    state.meta = meta
  }
}

export const actions = {
  async fetch (context, obj) {
    try {
      const { result } = await this.$api('meta.get', obj)
      if (result) {
        context.commit('setMeta', result)
      }
    } catch {
    }
  }
}
