import { v4 as uuidv4 } from 'uuid'

export const state = () => ({
  deviceId: uuidv4(),
  deviceIdCreatedAt: Date.now(),

  sessionId: uuidv4(),
  sessionIdCreatedAt: Date.now(),
  sessionsCount: 0,

  citySelected: false,
  cities: [
    { title: 'Санкт-Петербург', id: 1 },
    { title: 'Москва', id: 2 }
  ],
  city: 1,

  searchTypes: [
    { title: 'Найти арендатора', id: 1 },
    { title: 'Cнять гнездышко', id: 2 }
  ],
  searchType: 2,

  buildTypes: [
    { title: 'Студия', id: 0 },
    { title: 'Квартира', id: 1 },
    { title: 'Комната', id: 2 },
    { title: 'Дом', id: 3 }
  ],
  buildType: 0,

  draftAttributes: null,
  status: false,
  faveList: [],
  viewedList: [],
  notes: []
})

export const mutations = {
  setCity: (state, city) => (state.city = city),

  setCitySelected: (state, value) => (state.citySelected = value),

  setSearchType: (state, type) => (state.searchType = type),

  setBuildType: (state, type) => (state.buildType = type),

  setDraftAttributes: (state, draftAttributes) =>
    (state.draftAttributes = draftAttributes),

  toggleFave (state, offerId) {
    if (!state.faveList.includes(offerId)) {
      state.faveList.push(offerId)
    } else {
      state.faveList = state.faveList.filter(x => x !== offerId)
    }
  },

  addToViewed (state, offerId) {
    if (!state.viewedList.includes(offerId)) {
      state.viewedList.push(offerId)
    }
  },

  setDeviceId (state, deviceId) {
    if (state.deviceId === null) {
      state.deviceId = deviceId
      state.deviceIdCreatedAt = Date.now()
    }
  },

  setSessionId (state, sessionId) {
    if (
      state.sessionId === null ||
      state.sessionIdCreatedAt === null ||
      (Date.now() - state.sessionIdCreatedAt) / 1000 > 60 * 30 // reset session each 30 minutes
    ) {
      state.sessionId = sessionId
      state.sessionIdCreatedAt = Date.now()
      state.sessionsCount++
    }
  },

  setNote (state, note) {
    let found = false
    state.notes = state.notes.map((x) => {
      if (x.offerId === note.offerId) {
        x.text = note.text
        found = true
      }
      return x
    })
    if (!found) {
      state.notes.push(note)
    }
  }
}
