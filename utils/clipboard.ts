export default (text: string) => {
  const id = 'mycustom-clipboard-textarea-hidden-id'
  let existsTextarea = document.querySelector(`#${id}`)

  if (!existsTextarea) {
    const textarea = document.createElement('textarea')
    textarea.id = id
    textarea.style.position = 'fixed'
    textarea.style.top = '0px'
    textarea.style.left = '0px'

    // Ensure it has a small width and height. Setting to 1px / 1em
    // doesn't work as this gives a negative w/h on some browsers.
    textarea.style.width = '1px'
    textarea.style.height = '1px'

    // We don't need padding, reducing the size if it does flash render.
    textarea.style.padding = '0px'

    // Clean up any borders.
    textarea.style.border = 'none'
    textarea.style.outline = 'none'
    textarea.style.boxShadow = 'none'

    // Avoid flash of white box if rendered for any reason.
    textarea.style.background = 'transparent'
    const body = document.querySelector('body')
    if (body) {
      body.append(textarea)
      existsTextarea = document.querySelector(`#${id}`)
    }
  }

  if (existsTextarea) {
    // @ts-ignore
    existsTextarea.value = text
    // @ts-ignore
    existsTextarea.select()
  }

  try {
    return Boolean(document.execCommand('copy'))
  } catch {
    return false
  }
}
