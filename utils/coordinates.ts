export interface ICoordinates {
    latitude: number
    longitude: number
  }

export interface ICoordinateBounds {
    sw: ICoordinates
    ne: ICoordinates
  }

export function parseCoordinateBoundsFromRectangle (rectangle: string): ICoordinateBounds {
  const [sw, ne] = rectangle.split('|').map((part) => part.split(','))

  return {
    sw: {
      latitude: Number.parseFloat(sw[0]),
      longitude: Number.parseFloat(sw[1])
    },
    ne: {
      latitude: Number.parseFloat(ne[0]),
      longitude: Number.parseFloat(ne[1])
    }
  }
}
