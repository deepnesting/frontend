export default (phone) => {
  if (phone) {
    if (
      phone.charAt(0) !== '7' &&
            phone.charAt(0) !== '8' &&
            phone.charAt(0) !== '+'
    ) {
      phone = '7' + phone
    }
    if (phone.charAt(0) === '7') {
      phone = '+' + phone
    }
    if (phone.charAt(0) === '8') {
      phone = '+7' + phone.slice(1)
    }

    if (phone.length > 2 && phone.charAt(2) !== ' ') {
      phone = phone.slice(0, 2) + ' ' + phone.slice(2)
    }
    if (phone.length > 3 && phone.charAt(3) !== '(') {
      phone = phone.slice(0, 3) + '(' + phone.slice(3)
    }
    if (phone.length > 7 && phone.charAt(7) !== ')') {
      phone = phone.slice(0, 7) + ')' + phone.slice(7)
    }
    if (phone.length > 8 && phone.charAt(8) !== ' ') {
      phone = phone.slice(0, 8) + ' ' + phone.slice(8)
    }
    if (phone.length > 12 && phone.charAt(12) !== '-') {
      phone = phone.slice(0, 12) + '-' + phone.slice(12)
    }
    if (phone.length > 15 && phone.charAt(15) !== '-') {
      phone = phone.slice(0, 15) + '-' + phone.slice(15)
    }
    if (phone.length >= 18) {
      phone = phone.slice(0, 18)
    }
  } else {
    return phone
  }
  return phone
}
