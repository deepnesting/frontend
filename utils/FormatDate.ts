import { ru } from 'date-fns/locale'
import { formatRelative } from 'date-fns'

export default (time) => {
  try {
    return formatRelative(new Date(time), new Date(), { locale: ru })
  } catch (err) {
    console.log(err)

    return ''
  }
  // return `${time}`
}
