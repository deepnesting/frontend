export default async function ({ route, store }) {
  await store.dispatch('meta/fetch', { uri: route.fullPath })
}
