export default function ({ route, store, $api }) {
  const obj = {
    action: 'pageview',
    params: {
      pageViewParams: {
        routeName: route.name,
        routeParams: route.params,
        routeQuery: route.query,
        fullPath: route.fullPath,
        deviceIdCreatedAt: store.state.localStorage.deviceIdCreatedAt,
        sessionIdCreatedAt: store.state.localStorage.sessionIdCreatedAt
      }
    }
  }
  $api('analytics.save', obj)
}
