export default function ({ store, redirect, route }) {
  if (route.path.startsWith('/moderation')) {
    if (!(store.$auth.loggedIn && store.$auth.user.isModerator)) {
      return redirect('/')
    }
  }

  // internal pages who needs auth
  if (route.path.startsWith('/im') && !store.$auth.loggedIn) {
    return redirect('/auth/login')
  }

  if (route.path.includes('/edit') && !store.$auth.loggedIn) {
    return redirect('/auth/login')
  }
}
