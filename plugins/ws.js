import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

export default (ctx) => {
  if (process.client) {
    let url = `wss://${window.location.host}/api/v1/ws?rand=` + ctx.store.state.localStorage.deviceId
    if (process.env.NODE_ENV !== 'production') {
      url = 'ws://localhost:8000/api/v1/ws?rand=' + ctx.store.state.localStorage.deviceId
    }
    Vue.use(VueNativeSock, url, {
      store: ctx.store,
      type: 'json',
      reconnection: true,
      reconnectionDelay: 3000
    })
  }
}
