import { v4 as uuidv4 } from 'uuid'

import extend from '../utils/extend-vue-app.js'

export default function ({ app, store }) {
  extend(app, {
    mounted () {
      store.commit('localStorage/setSessionId', uuidv4())
      window.addEventListener('error', function (e) {
        const { message, source, lineno, colno, error } = e
        app.$api('analytics.handleError', {
          error: { message, source, lineno, colno, error, url: document.location.href }
        })
      })
    }
  })
}
