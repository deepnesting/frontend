// import axios from "axios";

const rpcBody = {
  jsonrpc: '2.0',
  id: 1
}

function makeSend (axios, store) {
  return function (method, params, axiosParams) {
    const newBody = { ...rpcBody, method, params }
    rpcBody.id++
    const res = axios.$post('/rpc/v1', newBody, {
      ...axiosParams,
      errorHandle: false,
      headers: {
        SessionID: store.state.localStorage.sessionId,
        DeviceID: store.state.localStorage.deviceId,
        LoadID: store.state.loadId
      }
    })
    return res
  }
}

export default ({ app, store }, inject) => {
  inject('api', makeSend(app.$axios, store))
}
