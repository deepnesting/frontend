// https://vuescrolljs.yvescoding.org/guide/#introduction
import Vue from 'vue'
import vuescroll from 'vuescroll/dist/vuescroll-native'
// import the css file
import 'vuescroll/dist/vuescroll.css'

Vue.use(vuescroll)
