/* eslint-disable unicorn/filename-case */
const storageFunction = (() => {
  // try {
  // const storage = {
  //   local: window.localStorage,
  //   session: window.sessionStorage
  // }
  // storage.local.setItem('__local_test', 1)
  // storage.local.removeItem('__local_test')

  return {
    local: {
      get: (name) => {
        return localStorage.getItem(name)
      },
      set: (name, val) => {
        return localStorage.setItem(name, val)
      }
    }
  }
  // } catch (e) {
  // console.error('unsopproted localStorage')
  // }
})()

export default (ctx, options) => {
  const store = ctx.store
  let localStoreNames = options.localStorage || ['localStorage']
  if (typeof options.localStorage === 'string') { localStoreNames = [options.localStorage] }

  const watchFunctionLocal = (i, val) => {
    const data = JSON.stringify(val)
    storageFunction.local.set(localStoreNames[i], data)
  }

  const watchHandlersLocal = []
  const watcherLocal = (name, i) => {
    return store.watch((state) => { return state[name] },
      val => watchFunctionLocal(i, val),
      { deep: true })
  }

  const bindLocalStorage = (name) => {
    let localPersist
    try {
      localPersist = JSON.parse(storageFunction.local.get(name))
    } catch {
      localPersist = null
    }

    console.log('bind storage', store.state)

    const data = { ...store.state }
    // если мы добавили новое поле, его нет в localStorage у старых пользователей
    // для обновления схемы делаем так:
    if (store.state[name] && localPersist && localPersist.deviceId) {
      // не обнуляем существующие поля:
      data[name] = Object.assign(data[name], localPersist)
      data[name].faveList = localPersist.faveList ?? []
      data[name].deviceId = localPersist.deviceId ?? ''
      data[name].sessionId = localPersist.sessionId ?? ''
      data[name].viewedList = localPersist.viewedList ?? []
      data[name].sessionsCount = localPersist.sessionsCount ?? 0

      // TODO: тут надо проверить что то что мы не хотели обнулить действительно было в localstorage
      if (!data[name].faveList) {
        data[name].faveList = []
      }
    }
    store.replaceState(data)

    localStoreNames.forEach((name, i) => {
      watchHandlersLocal[i] = watcherLocal(name, i)
    })
  }

  const watchOtherBrowsersLocalStorage = () => {
    window.addEventListener('storage', (event) => {
      if (event && event.storageArea === localStorage && Object.keys(store.state).includes(event.key)) {
        const data = { ...store.state }
        data[event.key] = JSON.parse(event.newValue)
        if (JSON.stringify(data) !== JSON.stringify(store.state)) { store.replaceState(data) }
      }
    })
  }

  localStoreNames.forEach((name, _) => {
    bindLocalStorage(name)
    watchOtherBrowsersLocalStorage()
  })
}
