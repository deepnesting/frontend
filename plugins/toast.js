export default (context, inject) => {
  const toast = { success: (e) => { alert(e) }, error: (e) => { alert(e) } }
  inject('toast', toast)
  context.$toast = toast
}
