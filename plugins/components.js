import Vue from 'vue'

import UgInput from '~/components/ui/UgInput.vue'
import UgFormRow from '~/components/ui/UgFormRow.vue'

/* Register common vue ui */
Vue.component('UgInput', UgInput)
Vue.component('UgFormRow', UgFormRow)
