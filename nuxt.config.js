const path = require('path')

module.exports = async () => {
  const config = {
    server: {
      host: '0.0.0.0'
    },
    ssr: false,
    target: 'static',
    /*
   ** Headers of the page
   */
    components: true,
    env: {
      isDev: process.env.NODE_ENV !== 'production'
    },
    head: {
      title: 'Уютное гнёздышко – найди своё место.',
      meta: [
        { charset: 'utf-8' },
        {
          hid: 'description',
          name: 'description',
          content:
          'Сообщество людей, где можно найти квартиру, комнату, жильца или соседа. Без агентов и комиссии.'
        },
        {
          name: 'og:title',
          content:
          'Уютное гнёздышко – поиск квартиры, комнаты, жильца или соседа.'
        },
        {
          property: 'og:description',
          name: 'og:description',
          content:
          'Сообщество людей, где можно найти квартиру, комнату, жильца или соседа. Без агентов и комиссии.'
        },
        {
          name: 'apple-mobile-web-app-title',
          content: 'Уютное гнёздышко'
        },
        {
          name: 'og:site_name',
          content: 'Уютное гнёздышко'
        },
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1, maximum-scale=1'
        },
        {
          name: 'yandex-verification',
          content: '433ceb1cf62700e8'
        }
      ],
      link: [
        {
          rel: 'icon',
          type: 'image/x-icon',
          href: '/favicon.ico'
        }
      ]
    },
    css: [
      '@/assets/styles/main.scss',
      'vue-slick-carousel/dist/vue-slick-carousel.css'
    ],
    render: {
      bundleRenderer: {
        shouldPreload: (file, type) => {
          return ['script', 'style', 'font'].includes(type)
        }
      }
    },
    /*
   ** Customize the progress bar color
   */
    loading: { color: '#038992' },
    modules: [
      '@nuxtjs/axios',
      '@nuxtjs/auth',
      '@nuxtjs/proxy',
      '@nuxtjs/device',
      '@nuxtjs/style-resources',
      '@nuxtjs/svg',
      ['@nuxtjs/pwa', { icon: false }],
      [
        '@nuxtjs/google-analytics',
        {
          id: 'UA-141108774-1'
        }
      ],
      [
        'vue-yandex-maps/nuxt',
        {
          apiKey: 'b8f5ffe3-18f0-4368-bae4-3cf3ea55f650',
          lang: 'ru_RU',
          version: '2.1'
        }
      ],
      '@nuxtjs/toast'
    ],
    toast: {
      position: 'top-center',
      duration: 5000
    },
    workbox: {
      importScripts: ['custom-sw.js?6']
    },
    styleResources: {
      scss: [
        './assets/styles/helpers/_variables.scss',
        './assets/styles/helpers/_mixins.scss'
      ]
    },
    plugins: [
      { src: '~/plugins/api.js' },
      // { src: '~/plugins/toast.js' },
      // { src: '~/plugins/vuescroll.js', ssr: true },
      // { src: '~/plugins/avatar.js', ssr: false },
      // { src: '~/plugins/swiper.js' },
      // { src: '~plugins/vue-js-toggle-button' },
      { src: '~/plugins/app.js', ssr: false },
      { src: '~/plugins/observe-visiblity.js' },
      { src: '~/plugins/ws.js', ssr: false },
      { src: '~/plugins/localStorage.js', ssr: false },
      { src: './plugins/vue-slick-carousel.js' }
    ],
    auth: {
      strategies: {
        local: {
          endpoints: {
            login: {
              url: '/api/v1/auth/login',
              method: 'post',
              propertyName: 'token'
            },
            logout: false, // { url: "/api/v1/auth/logout", method: "post" },
            user: {
              url: '/api/v1/users/me',
              method: 'get',
              propertyName: 'user'
            }
          },
          tokenRequired: true
        }
      },
      plugins: ['~/plugins/auth.js'],
      redirect: {
        login: false,
        logout: false,
        callback: false,
        home: false
      }
    },
    router: {
    // linkActiveClass: "is-active",
      middleware: ['offers', 'access', 'meta', 'analytics']
    },
    axios: {
      proxy: true
    // browserBaseURL: "",
    // baseURL: process.env.BASE_URL || "http://localhost:3000",
    },
    proxy: [
      [
        process.env.REST_PROXY_URL || 'https://dev.ugnest.com/api/v1',
        { changeOrigin: true, proxyTimeout: 60000, timeout: 60000 }
      ],
      [
        process.env.RPC_PROXY_URL || 'https://dev.ugnest.com/rpc/v1', // https://ugnest.com
        { changeOrigin: true, proxyTimeout: 60000, timeout: 60000 }
      ],
      [
        process.env.IMAGES_URL || 'https://dev.ugnest.com/images',
        { changeOrigin: true, proxyTimeout: 60000, timeout: 60000 }
      ]
    ],

    buildModules: [
      '@nuxtjs/web-vitals',
      // https://go.nuxtjs.dev/typescript
      '@nuxt/typescript-build'
      // https://go.nuxtjs.dev/stylelint
      // '@nuxtjs/stylelint-module',
      // 'nuxt-vite'
    ],

    webVitals: {
      provider: 'ga',
      debug: false,
      disabled: false
    },

    googleAnalytics: {
      id: 'UA-141108774-1'
    },

    typescript: {
      typeCheck: {
      // eslint: {
      //   silent: true
      //   // files: './**/*.{ts,js,vue}'
      // }
      }
    },

    /*
   ** Build configuration
   */
    // buildDir: "dist",
    build: {
      module: {
        rules: [
          {
            test: /\.scss$/i,
            use: ['style-loader', 'css-loader']
          }
        ]
      },
      analyze: false,
      extractCSS: true,
      extend (config, ctx) {
        if (ctx && ctx.isClient) {
          config.optimization.splitChunks.maxSize = 151200
        // TODO: удалить зависимость от nuxt-vuex-localstorage (он использует crypto, а он тяжелый)
        // config.node = false
        }
      },
      postcss: {
        'postcss-nested': {},
        'postcss-responsive-type': {},
        'postcss-hexrgba': {},
        'postcss-import': {}
      }
    }
  }

  return config
}
