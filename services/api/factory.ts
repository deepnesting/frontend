/* eslint-disable */
export interface IAnalyticsAddParams {
  actionType: number
}

export class AnalyticsAddParams implements IAnalyticsAddParams {
  static entityName = "analyticsaddparams";

  actionType: number = 0;
}

export interface IAnalyticsHandleErrorParams {
  error: object | null
}

export class AnalyticsHandleErrorParams implements IAnalyticsHandleErrorParams {
  static entityName = "analyticshandleerrorparams";

  error: object | null = null;
}

export interface IAnalyticsSaveParams {
  action: string | null,
  params: object | null
}

export class AnalyticsSaveParams implements IAnalyticsSaveParams {
  static entityName = "analyticssaveparams";

  action: string | null = null;
  params: object | null = null;
}

export interface IApiImage {
  attrSizes: string | null,
  attrSrcset: string | null,
  sizes: Array<IApiImageSize> | null,
  url: string | null
}

export class ApiImage implements IApiImage {
  static entityName = "apiimage";

  attrSizes: string | null = null;
  attrSrcset: string | null = null;
  sizes: Array<IApiImageSize> | null = [];
  url: string | null = null;
}

export interface IApiImageSize {
  url: string | null
}

export class ApiImageSize implements IApiImageSize {
  static entityName = "apiimagesize";

  url: string | null = null;
}

export interface IAuthCheckLoginParams {
  login: string | null
}

export class AuthCheckLoginParams implements IAuthCheckLoginParams {
  static entityName = "authcheckloginparams";

  login: string | null = null;
}

export interface IAuthCheckRestoreTokenParams {
  token: string | null
}

export class AuthCheckRestoreTokenParams implements IAuthCheckRestoreTokenParams {
  static entityName = "authcheckrestoretokenparams";

  token: string | null = null;
}

export interface IAuthConfirmRestoreParams {
  code: string | null,
  password: string | null,
  token: string | null
}

export class AuthConfirmRestoreParams implements IAuthConfirmRestoreParams {
  static entityName = "authconfirmrestoreparams";

  code: string | null = null;
  password: string | null = null;
  token: string | null = null;
}

export interface IAuthLoginParams {
  password: string | null,
  phone: string | null
}

export class AuthLoginParams implements IAuthLoginParams {
  static entityName = "authloginparams";

  password: string | null = null;
  phone: string | null = null;
}

export interface IAuthLoginWithVkParams {
  code: string | null,
  redirectURI: string | null
}

export class AuthLoginWithVkParams implements IAuthLoginWithVkParams {
  static entityName = "authloginwithvkparams";

  code: string | null = null;
  redirectURI: string | null = null;
}

export interface IAuthRegParams {
  code: number,
  isRestore: boolean,
  password: string | null,
  phone: string | null,
  sessionID: string | null
}

export class AuthRegParams implements IAuthRegParams {
  static entityName = "authregparams";

  code: number = 0;
  isRestore: boolean = false;
  password: string | null = null;
  phone: string | null = null;
  sessionID: string | null = null;
}

export interface IAuthSMSAuthParams {
  code: number,
  phone: string | null,
  sessionID: string | null
}

export class AuthSMSAuthParams implements IAuthSMSAuthParams {
  static entityName = "authsmsauthparams";

  code: number = 0;
  phone: string | null = null;
  sessionID: string | null = null;
}

export interface IAuthSendCodeParams {
  phone: string | null
}

export class AuthSendCodeParams implements IAuthSendCodeParams {
  static entityName = "authsendcodeparams";

  phone: string | null = null;
}

export interface IAuthSendRestoreParams {
  login: string | null
}

export class AuthSendRestoreParams implements IAuthSendRestoreParams {
  static entityName = "authsendrestoreparams";

  login: string | null = null;
}

export interface IBlacklist {
  createdAt: string | null,
  id: number,
  images: Array<IImage> | null,
  owner: IEmbedUser | null,
  reaction: IReaction | null,
  text: string | null,
  title: string | null
}

export class Blacklist implements IBlacklist {
  static entityName = "blacklist";

  createdAt: string | null = null;
  id: number = 0;
  images: Array<IImage> | null = [];
  owner: IEmbedUser | null = null;
  reaction: IReaction | null = null;
  text: string | null = null;
  title: string | null = null;
}

export interface IBlacklistDislikeParams {
  blacklistItemID: number
}

export class BlacklistDislikeParams implements IBlacklistDislikeParams {
  static entityName = "blacklistdislikeparams";

  blacklistItemID: number = 0;
}

export interface IBlacklistLikeParams {
  blacklistItemID: number
}

export class BlacklistLikeParams implements IBlacklistLikeParams {
  static entityName = "blacklistlikeparams";

  blacklistItemID: number = 0;
}

export interface IBlacklistRequestParams {
  images: Array<string> | null,
  reason: number,
  text: string | null
}

export class BlacklistRequestParams implements IBlacklistRequestParams {
  static entityName = "blacklistrequestparams";

  images: Array<string> | null = [];
  reason: number = 0;
  text: string | null = null;
}

export interface IBlogAddPostParams {
  blogPost: object | null
}

export class BlogAddPostParams implements IBlogAddPostParams {
  static entityName = "blogaddpostparams";

  blogPost: object | null = null;
}

export interface IBlogGetPostBySlugParams {
  slug: string | null
}

export class BlogGetPostBySlugParams implements IBlogGetPostBySlugParams {
  static entityName = "bloggetpostbyslugparams";

  slug: string | null = null;
}

export interface IBlogGetPostParams {
  postID: number
}

export class BlogGetPostParams implements IBlogGetPostParams {
  static entityName = "bloggetpostparams";

  postID: number = 0;
}

export interface IBlogPost {
  body: string | null,
  createdAt: string | null,
  id: number,
  publishedAt: string | null,
  shareText: string | null,
  slug: string | null,
  summary: string | null,
  title: string | null,
  topImage: IApiImage | null,
  updatedAt: string | null,
  viewCount: number
}

export class BlogPost implements IBlogPost {
  static entityName = "blogpost";

  body: string | null = null;
  createdAt: string | null = null;
  id: number = 0;
  publishedAt: string | null = null;
  shareText: string | null = null;
  slug: string | null = null;
  summary: string | null = null;
  title: string | null = null;
  topImage: IApiImage | null = null;
  updatedAt: string | null = null;
  viewCount: number = 0;
}

export interface IBlogPostsParams {
  page: number,
  pageSize: number
}

export class BlogPostsParams implements IBlogPostsParams {
  static entityName = "blogpostsparams";

  page: number = 0;
  pageSize: number = 0;
}

export interface IBlogUpdatePostParams {
  blogPost: object | null
}

export class BlogUpdatePostParams implements IBlogUpdatePostParams {
  static entityName = "blogupdatepostparams";

  blogPost: object | null = null;
}

export interface IBreadcrumb {
  title: string | null,
  url: string | null
}

export class Breadcrumb implements IBreadcrumb {
  static entityName = "breadcrumb";

  title: string | null = null;
  url: string | null = null;
}

export interface ICitiesAddParams {
  alias: string | null,
  shortTitle: string | null,
  title: string | null
}

export class CitiesAddParams implements ICitiesAddParams {
  static entityName = "citiesaddparams";

  alias: string | null = null;
  shortTitle: string | null = null;
  title: string | null = null;
}

export interface ICitiesGetParams {
  id: number
}

export class CitiesGetParams implements ICitiesGetParams {
  static entityName = "citiesgetparams";

  id: number = 0;
}

export interface ICitiesSaveParams {
  city: object | null
}

export class CitiesSaveParams implements ICitiesSaveParams {
  static entityName = "citiessaveparams";

  city: object | null = null;
}

export interface ICity {
  alias: string | null,
  hasMetro: boolean,
  id: number,
  lat: number,
  lon: number,
  metroIcon: string | null,
  prepositionalTitle: string | null,
  rect: ICity_Rect | null,
  shortTitle: string | null,
  status: number,
  telegramChannelName: string | null,
  title: string | null,
  vkPublicId: number,
  vkWebhookConfirmation: string | null
}

export class City implements ICity {
  static entityName = "city";

  alias: string | null = null;
  hasMetro: boolean = false;
  id: number = 0;
  lat: number = 0;
  lon: number = 0;
  metroIcon: string | null = null;
  prepositionalTitle: string | null = null;
  rect: ICity_Rect | null = null;
  shortTitle: string | null = null;
  status: number = 0;
  telegramChannelName: string | null = null;
  title: string | null = null;
  vkPublicId: number = 0;
  vkWebhookConfirmation: string | null = null;
}

export interface ICity_Rect {

}

export class City_Rect implements ICity_Rect {
  static entityName = "city_rect";


}

export interface ICommentsAddBlackilistCommentParams {
  blacklistItemID: number,
  comment: string | null
}

export class CommentsAddBlackilistCommentParams implements ICommentsAddBlackilistCommentParams {
  static entityName = "commentsaddblackilistcommentparams";

  blacklistItemID: number = 0;
  comment: string | null = null;
}

export interface ICommentsAddOfferCommentParams {
  comment: string | null,
  offerID: number
}

export class CommentsAddOfferCommentParams implements ICommentsAddOfferCommentParams {
  static entityName = "commentsaddoffercommentparams";

  comment: string | null = null;
  offerID: number = 0;
}

export interface ICommentsBlacklistCommentsParams {
  blacklistItemID: number
}

export class CommentsBlacklistCommentsParams implements ICommentsBlacklistCommentsParams {
  static entityName = "commentsblacklistcommentsparams";

  blacklistItemID: number = 0;
}

export interface ICommentsLatestParams {
  page: number,
  pageSize?: number
}

export class CommentsLatestParams implements ICommentsLatestParams {
  static entityName = "commentslatestparams";

  page: number = 0;
  pageSize?: number = 0;
}

export interface ICommentsListParams {
  objectID: number,
  objectType: number
}

export class CommentsListParams implements ICommentsListParams {
  static entityName = "commentslistparams";

  objectID: number = 0;
  objectType: number = 0;
}

export interface ICommentsOfferCommentsParams {
  offerID: number
}

export class CommentsOfferCommentsParams implements ICommentsOfferCommentsParams {
  static entityName = "commentsoffercommentsparams";

  offerID: number = 0;
}

export interface IContact {
  text: string | null,
  type: string | null,
  value: string | null
}

export class Contact implements IContact {
  static entityName = "contact";

  text: string | null = null;
  type: string | null = null;
  value: string | null = null;
}

export interface IDialog {
  id: number,
  pinned: boolean,
  text: string | null,
  time: string | null,
  title: string | null,
  type: number,
  unreadCount: number,
  user: IEmbedUser | null
}

export class Dialog implements IDialog {
  static entityName = "dialog";

  id: number = 0;
  pinned: boolean = false;
  text: string | null = null;
  time: string | null = null;
  title: string | null = null;
  type: number = 0;
  unreadCount: number = 0;
  user: IEmbedUser | null = null;
}

export interface IDraftsAddParams {
  address: string | null,
  buildType: number,
  images: Array<string> | null,
  price: number,
  roomCount: number,
  searchType: number,
  text: string | null
}

export class DraftsAddParams implements IDraftsAddParams {
  static entityName = "draftsaddparams";

  address: string | null = null;
  buildType: number = 0;
  images: Array<string> | null = [];
  price: number = 0;
  roomCount: number = 0;
  searchType: number = 0;
  text: string | null = null;
}

export interface IEmbedUser {
  avatarUrl: string | null,
  id: number,
  isBot: boolean,
  name: string | null,
  searchStatusText: string | null,
  username: string | null
}

export class EmbedUser implements IEmbedUser {
  static entityName = "embeduser";

  avatarUrl: string | null = null;
  id: number = 0;
  isBot: boolean = false;
  name: string | null = null;
  searchStatusText: string | null = null;
  username: string | null = null;
}

export interface IFastOfferSearch {
  area: Array<IGeoSearchResponseItem>,
  botOffers: boolean,
  bounds: Array<number>,
  buildType: number,
  city: number,
  ids: Array<number>,
  limit: number,
  metro: number,
  metros: Array<number>,
  offset: number,
  price: number,
  priceFrom: number,
  priceTo: number,
  rentDuration: number,
  roomCount: Array<number>,
  searchType: number,
  source: string,
  statuses: Array<number>,
  userId: number,
  zoom: number // Zoom for map cluster
}

export class FastOfferSearch implements IFastOfferSearch {
  static entityName = "fastoffersearch";

  area: Array<IGeoSearchResponseItem> = [];
  botOffers: boolean = false;
  bounds: Array<number> = [];
  buildType: number = 0;
  city: number = 0;
  ids: Array<number> = [];
  limit: number = 0;
  metro: number = 0;
  metros: Array<number> = [];
  offset: number = 0;
  price: number = 0;
  priceFrom: number = 0;
  priceTo: number = 0;
  rentDuration: number = 0;
  roomCount: Array<number> = [];
  searchType: number = 0;
  source: string = "";
  statuses: Array<number> = [];
  userId: number = 0;
  zoom: number = 0;
}

export interface IFastOfferUser {
  AvatarURL: string | null,
  FirstName: string | null,
  ID: number,
  IsVkUser: boolean,
  LastName: string | null,
  VkID: number,
  email: string | null
}

export class FastOfferUser implements IFastOfferUser {
  static entityName = "fastofferuser";

  AvatarURL: string | null = null;
  FirstName: string | null = null;
  ID: number = 0;
  IsVkUser: boolean = false;
  LastName: string | null = null;
  VkID: number = 0;
  email: string | null = null;
}

export interface IGeoExtractParams {
  text: string | null
}

export class GeoExtractParams implements IGeoExtractParams {
  static entityName = "geoextractparams";

  text: string | null = null;
}

export interface IGeoLocationParams {
  query: string | null
}

export class GeoLocationParams implements IGeoLocationParams {
  static entityName = "geolocationparams";

  query: string | null = null;
}

export interface IGeoSearchParams {
  cityID: number,
  q: string | null
}

export class GeoSearchParams implements IGeoSearchParams {
  static entityName = "geosearchparams";

  cityID: number = 0;
  q: string | null = null;
}

export interface IGeoSearchResponseItem {
  objectId: number,
  points: Array<IPoint> | null, // может быть несколько, например если это линия метро, то все метро этой линии
  shortTitle: string | null,
  title: string | null,
  type: string | null // Адрес / метро / линия метро / район
}

export class GeoSearchResponseItem implements IGeoSearchResponseItem {
  static entityName = "geosearchresponseitem";

  objectId: number = 0;
  points: Array<IPoint> | null = [];
  shortTitle: string | null = null;
  title: string | null = null;
  type: string | null = null;
}

export interface IGeoSuggestMetroStationParams {
  cityID: number,
  query: string | null
}

export class GeoSuggestMetroStationParams implements IGeoSuggestMetroStationParams {
  static entityName = "geosuggestmetrostationparams";

  cityID: number = 0;
  query: string | null = null;
}

export interface IGeoSuggestParams {
  query: string | null
}

export class GeoSuggestParams implements IGeoSuggestParams {
  static entityName = "geosuggestparams";

  query: string | null = null;
}

export interface IHelpChatMessage {
  createdAt: string | null,
  deviceId: string | null,
  fromModerator: boolean,
  id: number,
  text: string | null
}

export class HelpChatMessage implements IHelpChatMessage {
  static entityName = "helpchatmessage";

  createdAt: string | null = null;
  deviceId: string | null = null;
  fromModerator: boolean = false;
  id: number = 0;
  text: string | null = null;
}

export interface IHelpchatHistoryParams {
  deviceID: string | null,
  page: number,
  pageSize: number
}

export class HelpchatHistoryParams implements IHelpchatHistoryParams {
  static entityName = "helpchathistoryparams";

  deviceID: string | null = null;
  page: number = 0;
  pageSize: number = 0;
}

export interface IHelpchatSendParams {
  deviceID: string | null,
  text: string | null
}

export class HelpchatSendParams implements IHelpchatSendParams {
  static entityName = "helpchatsendparams";

  deviceID: string | null = null;
  text: string | null = null;
}

export interface IImage {
  color: string | null,
  hash: string | null,
  originalFileHash: string | null,
  url: string | null
}

export class Image implements IImage {
  static entityName = "image";

  color: string | null = null;
  hash: string | null = null;
  originalFileHash: string | null = null;
  url: string | null = null;
}

export interface IMessage {
  createdAt: string | null,
  id: number,
  image: string | null,
  outgoing: boolean,
  text: string | null,
  user: IEmbedUser | null
}

export class Message implements IMessage {
  static entityName = "message";

  createdAt: string | null = null;
  id: number = 0;
  image: string | null = null;
  outgoing: boolean = false;
  text: string | null = null;
  user: IEmbedUser | null = null;
}

export interface IMessengerHistoryParams {
  dialogID: number
}

export class MessengerHistoryParams implements IMessengerHistoryParams {
  static entityName = "messengerhistoryparams";

  dialogID: number = 0;
}

export interface IMessengerReadDialogParams {
  dialogID: number
}

export class MessengerReadDialogParams implements IMessengerReadDialogParams {
  static entityName = "messengerreaddialogparams";

  dialogID: number = 0;
}

export interface IMessengerResolveDialogIDParams {
  userID: number
}

export class MessengerResolveDialogIDParams implements IMessengerResolveDialogIDParams {
  static entityName = "messengerresolvedialogidparams";

  userID: number = 0;
}

export interface IMessengerSendMessageFromSupportParams {
  image: string | null,
  text: string | null,
  to: number
}

export class MessengerSendMessageFromSupportParams implements IMessengerSendMessageFromSupportParams {
  static entityName = "messengersendmessagefromsupportparams";

  image: string | null = null;
  text: string | null = null;
  to: number = 0;
}

export interface IMessengerSendParams {
  message?: object | null
}

export class MessengerSendParams implements IMessengerSendParams {
  static entityName = "messengersendparams";

  message?: object | null = null;
}

export interface IMessengerSendPrivateMessageParams {
  text: string | null,
  to: number
}

export class MessengerSendPrivateMessageParams implements IMessengerSendPrivateMessageParams {
  static entityName = "messengersendprivatemessageparams";

  text: string | null = null;
  to: number = 0;
}

export interface IMessengerSendTextParams {
  dialogID: number,
  image: string | null,
  text: string | null
}

export class MessengerSendTextParams implements IMessengerSendTextParams {
  static entityName = "messengersendtextparams";

  dialogID: number = 0;
  image: string | null = null;
  text: string | null = null;
}

export interface IMeta {
  breadcrumbs: Array<IValue> | null,
  description: string | null,
  image: string | null,
  pageType: string | null,
  searchOffers: IFastOfferSearch | null,
  title: string | null,
  twitterCard: string | null,
  twitterImage: string | null,
  url: string | null
}

export class Meta implements IMeta {
  static entityName = "meta";

  breadcrumbs: Array<IValue> | null = [];
  description: string | null = null;
  image: string | null = null;
  pageType: string | null = null;
  searchOffers: IFastOfferSearch | null = null;
  title: string | null = null;
  twitterCard: string | null = null;
  twitterImage: string | null = null;
  url: string | null = null;
}

export interface IMetaGetParams {
  uri: string | null
}

export class MetaGetParams implements IMetaGetParams {
  static entityName = "metagetparams";

  uri: string | null = null;
}

export interface IMetro {
  name: string | null
}

export class Metro implements IMetro {
  static entityName = "metro";

  name: string | null = null;
}

export interface IMetroStation {
  color: string | null,
  id: number,
  lat: number,
  lineId: number,
  lon: number,
  title: string | null
}

export class MetroStation implements IMetroStation {
  static entityName = "metrostation";

  color: string | null = null;
  id: number = 0;
  lat: number = 0;
  lineId: number = 0;
  lon: number = 0;
  title: string | null = null;
}

export interface IMetrosListParams {
  city: number
}

export class MetrosListParams implements IMetrosListParams {
  static entityName = "metroslistparams";

  city: number = 0;
}

export interface IModerationAnswerToHelpChatParams {
  deviceID: string | null,
  text: string | null
}

export class ModerationAnswerToHelpChatParams implements IModerationAnswerToHelpChatParams {
  static entityName = "moderationanswertohelpchatparams";

  deviceID: string | null = null;
  text: string | null = null;
}

export interface IModerationArchiveTicketParams {
  ticketID: number
}

export class ModerationArchiveTicketParams implements IModerationArchiveTicketParams {
  static entityName = "moderationarchiveticketparams";

  ticketID: number = 0;
}

export interface IModerationAvitoOffersParams {
  cityID: number
}

export class ModerationAvitoOffersParams implements IModerationAvitoOffersParams {
  static entityName = "moderationavitooffersparams";

  cityID: number = 0;
}

export interface IModerationBanOfferParams {
  comment: string | null,
  offerID: number
}

export class ModerationBanOfferParams implements IModerationBanOfferParams {
  static entityName = "moderationbanofferparams";

  comment: string | null = null;
  offerID: number = 0;
}

export interface IModerationCreateOfferFromTelegramParams {
  telegramOfferID: number
}

export class ModerationCreateOfferFromTelegramParams implements IModerationCreateOfferFromTelegramParams {
  static entityName = "moderationcreateofferfromtelegramparams";

  telegramOfferID: number = 0;
}

export interface IModerationDenialOfPublicationParams {
  message: string | null,
  offerID: number
}

export class ModerationDenialOfPublicationParams implements IModerationDenialOfPublicationParams {
  static entityName = "moderationdenialofpublicationparams";

  message: string | null = null;
  offerID: number = 0;
}

export interface IModerationDialogsParams {
  page: number,
  pageSize?: number
}

export class ModerationDialogsParams implements IModerationDialogsParams {
  static entityName = "moderationdialogsparams";

  page: number = 0;
  pageSize?: number = 0;
}

export interface IModerationFeedbackParams {
  message: string | null
}

export class ModerationFeedbackParams implements IModerationFeedbackParams {
  static entityName = "moderationfeedbackparams";

  message: string | null = null;
}

export interface IModerationHelpChatDialogsParams {
  page: number,
  pageSize?: number
}

export class ModerationHelpChatDialogsParams implements IModerationHelpChatDialogsParams {
  static entityName = "moderationhelpchatdialogsparams";

  page: number = 0;
  pageSize?: number = 0;
}

export interface IModerationOfferSubmitParams {
  offerID: number
}

export class ModerationOfferSubmitParams implements IModerationOfferSubmitParams {
  static entityName = "moderationoffersubmitparams";

  offerID: number = 0;
}

export interface IModerationPublishExternalOfferParams {
  address: string | null,
  buildType: number,
  id: string | null,
  integration: string | null,
  price: number,
  roomCount: number,
  searchType: number
}

export class ModerationPublishExternalOfferParams implements IModerationPublishExternalOfferParams {
  static entityName = "moderationpublishexternalofferparams";

  address: string | null = null;
  buildType: number = 0;
  id: string | null = null;
  integration: string | null = null;
  price: number = 0;
  roomCount: number = 0;
  searchType: number = 0;
}

export interface IModerationRejectExternalOfferParams {
  id: string | null,
  integration: string | null
}

export class ModerationRejectExternalOfferParams implements IModerationRejectExternalOfferParams {
  static entityName = "moderationrejectexternalofferparams";

  id: string | null = null;
  integration: string | null = null;
}

export interface IModerationRestartPublicationParams {
  offerID: number
}

export class ModerationRestartPublicationParams implements IModerationRestartPublicationParams {
  static entityName = "moderationrestartpublicationparams";

  offerID: number = 0;
}

export interface IModerationSubmitOfferParams {
  offerID: number
}

export class ModerationSubmitOfferParams implements IModerationSubmitOfferParams {
  static entityName = "moderationsubmitofferparams";

  offerID: number = 0;
}

export interface IModerationTicketRejectParams {
  ticketID: number
}

export class ModerationTicketRejectParams implements IModerationTicketRejectParams {
  static entityName = "moderationticketrejectparams";

  ticketID: number = 0;
}

export interface IModerationTicketSetViewedByModeratorParams {
  ticketID: number
}

export class ModerationTicketSetViewedByModeratorParams implements IModerationTicketSetViewedByModeratorParams {
  static entityName = "moderationticketsetviewedbymoderatorparams";

  ticketID: number = 0;
}

export interface IModerationTicketSubmitParams {
  comment: string | null,
  ticketID: number
}

export class ModerationTicketSubmitParams implements IModerationTicketSubmitParams {
  static entityName = "moderationticketsubmitparams";

  comment: string | null = null;
  ticketID: number = 0;
}

export interface IModerationUsersParams {
  page: number,
  pageSize: number,
  q: string | null
}

export class ModerationUsersParams implements IModerationUsersParams {
  static entityName = "moderationusersparams";

  page: number = 0;
  pageSize: number = 0;
  q: string | null = null;
}

export interface IOffer {
  address: string | null,
  breadcrumb: Array<IBreadcrumb> | null,
  buildType: number,
  city: number,
  comment: string | null,
  communal: number,
  externalId: string | null,
  hasCommunal: boolean,
  images: Array<IApiImage> | null,
  isPhoneContact: boolean, // показывает, какой тип связи предпочитает собственник. Если true, то к объявлению привязан номер телефона, иначе
//будет ссылка на соцсеть
  isVkOffer: boolean,
  lat: number,
  lon: number,
  metro: number,
  metroStations: Array<IMetroStation> | null,
  offerId: number,
  paid: boolean,
  paidAt: string | null,
  price: number,
  publishedAt: string | null,
  publishedUntil: string | null,
  rentDuration: number,
  roomCount: number,
  searchType: number,
  status: number,
  statusComment: string | null,
  statusTitle: string | null,
  title: string | null,
  user: IUser | null,
  vkID: number,
  vkOwnerId: number
}

export class Offer implements IOffer {
  static entityName = "offer";

  address: string | null = null;
  breadcrumb: Array<IBreadcrumb> | null = [];
  buildType: number = 0;
  city: number = 0;
  comment: string | null = null;
  communal: number = 0;
  externalId: string | null = null;
  hasCommunal: boolean = false;
  images: Array<IApiImage> | null = [];
  isPhoneContact: boolean = false;
  isVkOffer: boolean = false;
  lat: number = 0;
  lon: number = 0;
  metro: number = 0;
  metroStations: Array<IMetroStation> | null = [];
  offerId: number = 0;
  paid: boolean = false;
  paidAt: string | null = null;
  price: number = 0;
  publishedAt: string | null = null;
  publishedUntil: string | null = null;
  rentDuration: number = 0;
  roomCount: number = 0;
  searchType: number = 0;
  status: number = 0;
  statusComment: string | null = null;
  statusTitle: string | null = null;
  title: string | null = null;
  user: IUser | null = null;
  vkID: number = 0;
  vkOwnerId: number = 0;
}

export interface IOfferCard {
  buildType: number,
  comment: string | null,
  commentCount: number,
  hasPhone: boolean,
  id: number,
  images: Array<IApiImage> | null,
  isFavorite: boolean,
  isVkOffer: boolean,
  lat: number,
  lon: number,
  metroStations: Array<IMetroStation> | null,
  price: number,
  publishedAt: string | null,
  reactions: IReaction | null,
  searchType: number,
  status: string | null,
  statusId: number,
  title: string | null,
  user: IEmbedUser | null,
  views: number,
  vkViews: number
}

export class OfferCard implements IOfferCard {
  static entityName = "offercard";

  buildType: number = 0;
  comment: string | null = null;
  commentCount: number = 0;
  hasPhone: boolean = false;
  id: number = 0;
  images: Array<IApiImage> | null = [];
  isFavorite: boolean = false;
  isVkOffer: boolean = false;
  lat: number = 0;
  lon: number = 0;
  metroStations: Array<IMetroStation> | null = [];
  price: number = 0;
  publishedAt: string | null = null;
  reactions: IReaction | null = null;
  searchType: number = 0;
  status: string | null = null;
  statusId: number = 0;
  title: string | null = null;
  user: IEmbedUser | null = null;
  views: number = 0;
  vkViews: number = 0;
}

export interface IOffersAddToFaveParams {
  offerID: number
}

export class OffersAddToFaveParams implements IOffersAddToFaveParams {
  static entityName = "offersaddtofaveparams";

  offerID: number = 0;
}

export interface IOffersCancelSubscriptionByTelegramUserParams {
  telegramUserID: number
}

export class OffersCancelSubscriptionByTelegramUserParams implements IOffersCancelSubscriptionByTelegramUserParams {
  static entityName = "offerscancelsubscriptionbytelegramuserparams";

  telegramUserID: number = 0;
}

export interface IOffersCheckExistsParams {
  source: string | null
}

export class OffersCheckExistsParams implements IOffersCheckExistsParams {
  static entityName = "offerscheckexistsparams";

  source: string | null = null;
}

export interface IOffersComplaintParams {
  objectID: number,
  objectType: number,
  reason: number,
  text: string | null
}

export class OffersComplaintParams implements IOffersComplaintParams {
  static entityName = "offerscomplaintparams";

  objectID: number = 0;
  objectType: number = 0;
  reason: number = 0;
  text: string | null = null;
}

export interface IOffersContactsParams {
  offerID: number
}

export class OffersContactsParams implements IOffersContactsParams {
  static entityName = "offerscontactsparams";

  offerID: number = 0;
}

export interface IOffersCreateDealRequestParams {
  comment: string | null,
  endAt: string | null,
  offerID: number,
  startAt: string | null
}

export class OffersCreateDealRequestParams implements IOffersCreateDealRequestParams {
  static entityName = "offerscreatedealrequestparams";

  comment: string | null = null;
  endAt: string | null = null;
  offerID: number = 0;
  startAt: string | null = null;
}

export interface IOffersDislikeParams {
  offerID: number
}

export class OffersDislikeParams implements IOffersDislikeParams {
  static entityName = "offersdislikeparams";

  offerID: number = 0;
}

export interface IOffersFastCardsParams {
  search: object | null
}

export class OffersFastCardsParams implements IOffersFastCardsParams {
  static entityName = "offersfastcardsparams";

  search: object | null = null;
}

export interface IOffersFastCreateParams {
  offerRequest: object | null
}

export class OffersFastCreateParams implements IOffersFastCreateParams {
  static entityName = "offersfastcreateparams";

  offerRequest: object | null = null;
}

export interface IOffersFastUpdateParams {
  offerRequest: object | null
}

export class OffersFastUpdateParams implements IOffersFastUpdateParams {
  static entityName = "offersfastupdateparams";

  offerRequest: object | null = null;
}

export interface IOffersGeoSearchParams {
  search: object | null
}

export class OffersGeoSearchParams implements IOffersGeoSearchParams {
  static entityName = "offersgeosearchparams";

  search: object | null = null;
}

export interface IOffersGetFastParams {
  offerID: number
}

export class OffersGetFastParams implements IOffersGetFastParams {
  static entityName = "offersgetfastparams";

  offerID: number = 0;
}

export interface IOffersLikeParams {
  offerID: number
}

export class OffersLikeParams implements IOffersLikeParams {
  static entityName = "offerslikeparams";

  offerID: number = 0;
}

export interface IOffersOfferRemoveFromFaveParams {
  offerID: number
}

export class OffersOfferRemoveFromFaveParams implements IOffersOfferRemoveFromFaveParams {
  static entityName = "offersofferremovefromfaveparams";

  offerID: number = 0;
}

export interface IOffersParseParams {
  cityID: number,
  text: string | null
}

export class OffersParseParams implements IOffersParseParams {
  static entityName = "offersparseparams";

  cityID: number = 0;
  text: string | null = null;
}

export interface IOffersPrepareSearchInfoParams {
  search: object | null
}

export class OffersPrepareSearchInfoParams implements IOffersPrepareSearchInfoParams {
  static entityName = "offerspreparesearchinfoparams";

  search: object | null = null;
}

export interface IOffersProxyParams {
  uri: string | null
}

export class OffersProxyParams implements IOffersProxyParams {
  static entityName = "offersproxyparams";

  uri: string | null = null;
}

export interface IOffersPublicUserOffersParams {
  userID: number
}

export class OffersPublicUserOffersParams implements IOffersPublicUserOffersParams {
  static entityName = "offerspublicuseroffersparams";

  userID: number = 0;
}

export interface IOffersReactionsParams {
  offerID: number
}

export class OffersReactionsParams implements IOffersReactionsParams {
  static entityName = "offersreactionsparams";

  offerID: number = 0;
}

export interface IOffersRotateImageParams {
  uri: string | null
}

export class OffersRotateImageParams implements IOffersRotateImageParams {
  static entityName = "offersrotateimageparams";

  uri: string | null = null;
}

export interface IOffersSearchByImageParams {
  imageID: string | null
}

export class OffersSearchByImageParams implements IOffersSearchByImageParams {
  static entityName = "offerssearchbyimageparams";

  imageID: string | null = null;
}

export interface IOffersShowContactsParams {
  offerID: number
}

export class OffersShowContactsParams implements IOffersShowContactsParams {
  static entityName = "offersshowcontactsparams";

  offerID: number = 0;
}

export interface IOffersSubmitForModerationParams {
  offerID: number
}

export class OffersSubmitForModerationParams implements IOffersSubmitForModerationParams {
  static entityName = "offerssubmitformoderationparams";

  offerID: number = 0;
}

export interface IOffersSubscribeSearchTelegramUserParams {
  searchHash: string | null,
  telegramUserID: number
}

export class OffersSubscribeSearchTelegramUserParams implements IOffersSubscribeSearchTelegramUserParams {
  static entityName = "offerssubscribesearchtelegramuserparams";

  searchHash: string | null = null;
  telegramUserID: number = 0;
}

export interface IOffersUnPublishParams {
  offerID: number
}

export class OffersUnPublishParams implements IOffersUnPublishParams {
  static entityName = "offersunpublishparams";

  offerID: number = 0;
}

export interface IOffersUpdateDealStatusParams {
  dealID: number,
  newComment: string | null,
  newStatus: number
}

export class OffersUpdateDealStatusParams implements IOffersUpdateDealStatusParams {
  static entityName = "offersupdatedealstatusparams";

  dealID: number = 0;
  newComment: string | null = null;
  newStatus: number = 0;
}

export interface IPageViewParams {
  deviceId: string | null,
  deviceIdCreatedAt: number,
  fullPath: string | null,
  routeName: string | null,
  routeParams: object | null,
  routeQuery: object | null,
  sessionId: string | null,
  sessionIdCreatedAt: number
}

export class PageViewParams implements IPageViewParams {
  static entityName = "pageviewparams";

  deviceId: string | null = null;
  deviceIdCreatedAt: number = 0;
  fullPath: string | null = null;
  routeName: string | null = null;
  routeParams: object | null = null;
  routeQuery: object | null = null;
  sessionId: string | null = null;
  sessionIdCreatedAt: number = 0;
}

export interface IPaymentMethod {
  icon: string | null,
  redirectURL: string | null
}

export class PaymentMethod implements IPaymentMethod {
  static entityName = "paymentmethod";

  icon: string | null = null;
  redirectURL: string | null = null;
}

export interface IPaymentsInvoiceParams {
  offerId: number
}

export class PaymentsInvoiceParams implements IPaymentsInvoiceParams {
  static entityName = "paymentsinvoiceparams";

  offerId: number = 0;
}

export interface IPoint {
  count: number,
  geoId: string | null,
  id: number,
  items: Array<number> | null,
  lat: number,
  lon: number,
  radius: number, // в километрах, например, если это район, то 5км, если станция метро, то 1км
  type: string | null
}

export class Point implements IPoint {
  static entityName = "point";

  count: number = 0;
  geoId: string | null = null;
  id: number = 0;
  items: Array<number> | null = [];
  lat: number = 0;
  lon: number = 0;
  radius: number = 0;
  type: string | null = null;
}

export interface IReaction {
  count: Array<IReactionCount> | null,
  dislikes: number,
  likes: number,
  reacted: boolean,
  reaction: number
}

export class Reaction implements IReaction {
  static entityName = "reaction";

  count: Array<IReactionCount> | null = [];
  dislikes: number = 0;
  likes: number = 0;
  reacted: boolean = false;
  reaction: number = 0;
}

export interface IReactionCount {
  count: number,
  reaction: number
}

export class ReactionCount implements IReactionCount {
  static entityName = "reactioncount";

  count: number = 0;
  reaction: number = 0;
}

export interface IStreet {
  name: string | null,
  type: string | null
}

export class Street implements IStreet {
  static entityName = "street";

  name: string | null = null;
  type: string | null = null;
}

export interface ISuggestion {
  id: number,
  title: string | null
}

export class Suggestion implements ISuggestion {
  static entityName = "suggestion";

  id: number = 0;
  title: string | null = null;
}

export interface ITicket {
  createdAt: string | null,
  id: number,
  objectLink: string | null,
  status: number,
  statusText: string | null,
  text: string | null,
  title: string | null
}

export class Ticket implements ITicket {
  static entityName = "ticket";

  createdAt: string | null = null;
  id: number = 0;
  objectLink: string | null = null;
  status: number = 0;
  statusText: string | null = null;
  text: string | null = null;
  title: string | null = null;
}

export interface ITicketsApproveParams {
  id: number
}

export class TicketsApproveParams implements ITicketsApproveParams {
  static entityName = "ticketsapproveparams";

  id: number = 0;
}

export interface ITicketsGetParams {
  id: number
}

export class TicketsGetParams implements ITicketsGetParams {
  static entityName = "ticketsgetparams";

  id: number = 0;
}

export interface ITicketsListParams {
  search: object | null
}

export class TicketsListParams implements ITicketsListParams {
  static entityName = "ticketslistparams";

  search: object | null = null;
}

export interface ITicketsRejectParams {
  id: number,
  msg: string | null,
  reasonID: number
}

export class TicketsRejectParams implements ITicketsRejectParams {
  static entityName = "ticketsrejectparams";

  id: number = 0;
  msg: string | null = null;
  reasonID: number = 0;
}

export interface IUser {
  age: number,
  animalTitle: string | null,
  avatarUrl: string | null,
  bdate: string | null,
  bio: string | null,
  cityId: number,
  cityTitle: string | null,
  createdAt: string | null,
  email: string | null,
  firstName: string | null,
  hasChild: boolean,
  hometown: string | null,
  id: number,
  isAdmin: boolean,
  isBot: boolean,
  isModerator: boolean,
  isSupervisor: boolean,
  isVkUser: boolean,
  lastName: string | null,
  password: string | null,
  pluralAge: string | null,
  relation: number,
  relationTitle: string | null,
  sex: number,
  sexTitle: string | null,
  username: string | null,
  vkId: number
}

export class User implements IUser {
  static entityName = "user";

  age: number = 0;
  animalTitle: string | null = null;
  avatarUrl: string | null = null;
  bdate: string | null = null;
  bio: string | null = null;
  cityId: number = 0;
  cityTitle: string | null = null;
  createdAt: string | null = null;
  email: string | null = null;
  firstName: string | null = null;
  hasChild: boolean = false;
  hometown: string | null = null;
  id: number = 0;
  isAdmin: boolean = false;
  isBot: boolean = false;
  isModerator: boolean = false;
  isSupervisor: boolean = false;
  isVkUser: boolean = false;
  lastName: string | null = null;
  password: string | null = null;
  pluralAge: string | null = null;
  relation: number = 0;
  relationTitle: string | null = null;
  sex: number = 0;
  sexTitle: string | null = null;
  username: string | null = null;
  vkId: number = 0;
}

export interface IUsersAddWebSubscriptionParams {
  subscription: object | null
}

export class UsersAddWebSubscriptionParams implements IUsersAddWebSubscriptionParams {
  static entityName = "usersaddwebsubscriptionparams";

  subscription: object | null = null;
}

export interface IUsersProfileParams {
  userID: number
}

export class UsersProfileParams implements IUsersProfileParams {
  static entityName = "usersprofileparams";

  userID: number = 0;
}

export interface IUsersPromoteParams {
  role: object | null,
  userID: number
}

export class UsersPromoteParams implements IUsersPromoteParams {
  static entityName = "userspromoteparams";

  role: object | null = null;
  userID: number = 0;
}

export interface IUsersRequestModerationRightsParams {
  user?: object | null
}

export class UsersRequestModerationRightsParams implements IUsersRequestModerationRightsParams {
  static entityName = "usersrequestmoderationrightsparams";

  user?: object | null = null;
}

export interface IUsersSearchParams {
  search: object | null
}

export class UsersSearchParams implements IUsersSearchParams {
  static entityName = "userssearchparams";

  search: object | null = null;
}

export interface IUsersUpdateParams {
  user: object | null
}

export class UsersUpdateParams implements IUsersUpdateParams {
  static entityName = "usersupdateparams";

  user: object | null = null;
}

export interface IUsersUpdateProfileDataParams {
  bdate: string | null,
  htown: string | null,
  info: string | null
}

export class UsersUpdateProfileDataParams implements IUsersUpdateProfileDataParams {
  static entityName = "usersupdateprofiledataparams";

  bdate: string | null = null;
  htown: string | null = null;
  info: string | null = null;
}

export interface IUsersUpdateProfileParams {
  profile: object | null
}

export class UsersUpdateProfileParams implements IUsersUpdateProfileParams {
  static entityName = "usersupdateprofileparams";

  profile: object | null = null;
}

export interface IUsersUserByIDParams {
  userID: number
}

export class UsersUserByIDParams implements IUsersUserByIDParams {
  static entityName = "usersuserbyidparams";

  userID: number = 0;
}

export interface IValue {
  title: string | null,
  value: string | null
}

export class Value implements IValue {
  static entityName = "value";

  title: string | null = null;
  value: string | null = null;
}

export const factory = (send: any) => ({
  analytics: {
    add(params: IAnalyticsAddParams): Promise<void> {
      return send('analytics.Add', params)
    },
    createOfferStats(): Promise<Array<number>> {
      return send('analytics.CreateOfferStats')
    },
    handleError(params: IAnalyticsHandleErrorParams): Promise<void> {
      return send('analytics.HandleError', params)
    },
    save(params: IAnalyticsSaveParams): Promise<void> {
      return send('analytics.Save', params)
    }
  },
  app: {
    settings(): Promise<object> {
      return send('app.Settings')
    }
  },
  auth: {
    checkLogin(params: IAuthCheckLoginParams): Promise<boolean> {
      return send('auth.CheckLogin', params)
    },
    checkRestoreToken(params: IAuthCheckRestoreTokenParams): Promise<void> {
      return send('auth.CheckRestoreToken', params)
    },
    confirmRestore(params: IAuthConfirmRestoreParams): Promise<object> {
      return send('auth.ConfirmRestore', params)
    },
    login(params: IAuthLoginParams): Promise<object> {
      return send('auth.Login', params)
    },
    loginWithVk(params: IAuthLoginWithVkParams): Promise<object> {
      return send('auth.LoginWithVk', params)
    },
    reg(params: IAuthRegParams): Promise<object> {
      return send('auth.Reg', params)
    },
    sMSAuth(params: IAuthSMSAuthParams): Promise<object> {
      return send('auth.SMSAuth', params)
    },
    sendCode(params: IAuthSendCodeParams): Promise<string> {
      return send('auth.SendCode', params)
    },
    sendRestore(params: IAuthSendRestoreParams): Promise<string> {
      return send('auth.SendRestore', params)
    }
  },
  blacklist: {
    dislike(params: IBlacklistDislikeParams): Promise<void> {
      return send('blacklist.Dislike', params)
    },
    like(params: IBlacklistLikeParams): Promise<void> {
      return send('blacklist.Like', params)
    },
    list(): Promise<Array<IBlacklist>> {
      return send('blacklist.List')
    },
    pending(): Promise<Array<IBlacklist>> {
      return send('blacklist.Pending')
    },
    request(params: IBlacklistRequestParams): Promise<number> {
      return send('blacklist.Request', params)
    }
  },
  blog: {
    addPost(params: IBlogAddPostParams): Promise<number> {
      return send('blog.AddPost', params)
    },
    getPost(params: IBlogGetPostParams): Promise<object> {
      return send('blog.GetPost', params)
    },
    getPostBySlug(params: IBlogGetPostBySlugParams): Promise<object> {
      return send('blog.GetPostBySlug', params)
    },
    posts(params: IBlogPostsParams): Promise<Array<IBlogPost>> {
      return send('blog.Posts', params)
    },
    updatePost(params: IBlogUpdatePostParams): Promise<void> {
      return send('blog.UpdatePost', params)
    }
  },
  cities: {
    add(params: ICitiesAddParams): Promise<number> {
      return send('cities.Add', params)
    },
    all(): Promise<Array<ICity>> {
      return send('cities.All')
    },
    get(params: ICitiesGetParams): Promise<object> {
      return send('cities.Get', params)
    },
    save(params: ICitiesSaveParams): Promise<number> {
      return send('cities.Save', params)
    }
  },
  comments: {
    addBlackilistComment(params: ICommentsAddBlackilistCommentParams): Promise<number> {
      return send('comments.AddBlackilistComment', params)
    },
    addOfferComment(params: ICommentsAddOfferCommentParams): Promise<number> {
      return send('comments.AddOfferComment', params)
    },
    blacklistComments(params: ICommentsBlacklistCommentsParams): Promise<object> {
      return send('comments.BlacklistComments', params)
    },
    latest(params: ICommentsLatestParams): Promise<object> {
      return send('comments.Latest', params)
    },
    list(params: ICommentsListParams): Promise<object> {
      return send('comments.List', params)
    },
    offerComments(params: ICommentsOfferCommentsParams): Promise<object> {
      return send('comments.OfferComments', params)
    }
  },
  drafts: {
    add(params: IDraftsAddParams): Promise<number> {
      return send('drafts.Add', params)
    }
  },
  geo: {
    extract(params: IGeoExtractParams): Promise<object> {
      return send('geo.Extract', params)
    },
    location(params: IGeoLocationParams): Promise<object> {
      return send('geo.Location', params)
    },
    search(params: IGeoSearchParams): Promise<Array<IGeoSearchResponseItem>> {
      return send('geo.Search', params)
    },
    suggest(params: IGeoSuggestParams): Promise<Array<ISuggestion>> {
      return send('geo.Suggest', params)
    },
    suggestMetroStation(params: IGeoSuggestMetroStationParams): Promise<Array<ISuggestion>> {
      return send('geo.SuggestMetroStation', params)
    }
  },
  helpchat: {
    history(params: IHelpchatHistoryParams): Promise<Array<IHelpChatMessage>> {
      return send('helpchat.History', params)
    },
    send(params: IHelpchatSendParams): Promise<number> {
      return send('helpchat.Send', params)
    }
  },
  messenger: {
    dialogWithSupport(): Promise<object> {
      return send('messenger.DialogWithSupport')
    },
    dialogs(): Promise<Array<IDialog>> {
      return send('messenger.Dialogs')
    },
    history(params: IMessengerHistoryParams): Promise<Array<IMessage>> {
      return send('messenger.History', params)
    },
    readDialog(params: IMessengerReadDialogParams): Promise<void> {
      return send('messenger.ReadDialog', params)
    },
    resolveDialogID(params: IMessengerResolveDialogIDParams): Promise<number> {
      return send('messenger.ResolveDialogID', params)
    },
    send(params: IMessengerSendParams): Promise<number> {
      return send('messenger.Send', params)
    },
    sendMessageFromSupport(params: IMessengerSendMessageFromSupportParams): Promise<number> {
      return send('messenger.SendMessageFromSupport', params)
    },
    sendPrivateMessage(params: IMessengerSendPrivateMessageParams): Promise<object> {
      return send('messenger.SendPrivateMessage', params)
    },
    sendText(params: IMessengerSendTextParams): Promise<number> {
      return send('messenger.SendText', params)
    },
    unreadCount(): Promise<number> {
      return send('messenger.UnreadCount')
    }
  },
  meta: {
    get(params: IMetaGetParams): Promise<object> {
      return send('meta.Get', params)
    }
  },
  metros: {
    list(params: IMetrosListParams): Promise<Array<IMetroStation>> {
      return send('metros.List', params)
    }
  },
  moderation: {
    answerToHelpChat(params: IModerationAnswerToHelpChatParams): Promise<number> {
      return send('moderation.AnswerToHelpChat', params)
    },
    archiveTicket(params: IModerationArchiveTicketParams): Promise<void> {
      return send('moderation.ArchiveTicket', params)
    },
    avitoOffers(params: IModerationAvitoOffersParams): Promise<Array<IOffer>> {
      return send('moderation.AvitoOffers', params)
    },
    banOffer(params: IModerationBanOfferParams): Promise<void> {
      return send('moderation.BanOffer', params)
    },
    createOfferFromTelegram(params: IModerationCreateOfferFromTelegramParams): Promise<number> {
      return send('moderation.CreateOfferFromTelegram', params)
    },
    denialOfPublication(params: IModerationDenialOfPublicationParams): Promise<void> {
      return send('moderation.DenialOfPublication', params)
    },
    dialogs(params: IModerationDialogsParams): Promise<Array<IDialog>> {
      return send('moderation.Dialogs', params)
    },
    feedback(params: IModerationFeedbackParams): Promise<void> {
      return send('moderation.Feedback', params)
    },
    helpChatDialogs(params: IModerationHelpChatDialogsParams): Promise<Array<IHelpChatMessage>> {
      return send('moderation.HelpChatDialogs', params)
    },
    levelUpRequest(): Promise<void> {
      return send('moderation.LevelUpRequest')
    },
    offerSubmit(params: IModerationOfferSubmitParams): Promise<void> {
      return send('moderation.OfferSubmit', params)
    },
    publishExternalOffer(params: IModerationPublishExternalOfferParams): Promise<number> {
      return send('moderation.PublishExternalOffer', params)
    },
    rejectExternalOffer(params: IModerationRejectExternalOfferParams): Promise<void> {
      return send('moderation.RejectExternalOffer', params)
    },
    restartPublication(params: IModerationRestartPublicationParams): Promise<void> {
      return send('moderation.RestartPublication', params)
    },
    submitOffer(params: IModerationSubmitOfferParams): Promise<void> {
      return send('moderation.SubmitOffer', params)
    },
    telegramOffers(): Promise<Array<IOffer>> {
      return send('moderation.TelegramOffers')
    },
    ticketReject(params: IModerationTicketRejectParams): Promise<void> {
      return send('moderation.TicketReject', params)
    },
    ticketSetViewedByModerator(params: IModerationTicketSetViewedByModeratorParams): Promise<void> {
      return send('moderation.TicketSetViewedByModerator', params)
    },
    ticketSubmit(params: IModerationTicketSubmitParams): Promise<void> {
      return send('moderation.TicketSubmit', params)
    },
    users(params: IModerationUsersParams): Promise<Array<IUser>> {
      return send('moderation.Users', params)
    }
  },
  offers: {
    addToFave(params: IOffersAddToFaveParams): Promise<void> {
      return send('offers.AddToFave', params)
    },
    cancelSubscriptionByTelegramUser(params: IOffersCancelSubscriptionByTelegramUserParams): Promise<void> {
      return send('offers.CancelSubscriptionByTelegramUser', params)
    },
    checkExists(params: IOffersCheckExistsParams): Promise<boolean> {
      return send('offers.CheckExists', params)
    },
    complaint(params: IOffersComplaintParams): Promise<number> {
      return send('offers.Complaint', params)
    },
    contacts(params: IOffersContactsParams): Promise<Array<IContact>> {
      return send('offers.Contacts', params)
    },
    createDealRequest(params: IOffersCreateDealRequestParams): Promise<number> {
      return send('offers.CreateDealRequest', params)
    },
    dislike(params: IOffersDislikeParams): Promise<void> {
      return send('offers.Dislike', params)
    },
    disliked(): Promise<Array<IOffer>> {
      return send('offers.Disliked')
    },
    fastCards(params: IOffersFastCardsParams): Promise<object> {
      return send('offers.FastCards', params)
    },
    fastCreate(params: IOffersFastCreateParams): Promise<number> {
      return send('offers.FastCreate', params)
    },
    fastUpdate(params: IOffersFastUpdateParams): Promise<void> {
      return send('offers.FastUpdate', params)
    },
    geoSearch(params: IOffersGeoSearchParams): Promise<Array<IPoint>> {
      return send('offers.GeoSearch', params)
    },
    getFast(params: IOffersGetFastParams): Promise<object> {
      return send('offers.GetFast', params)
    },
    like(params: IOffersLikeParams): Promise<void> {
      return send('offers.Like', params)
    },
    liked(): Promise<Array<IOffer>> {
      return send('offers.Liked')
    },
    offerRemoveFromFave(params: IOffersOfferRemoveFromFaveParams): Promise<void> {
      return send('offers.OfferRemoveFromFave', params)
    },
    offersByUser(): Promise<object> {
      return send('offers.OffersByUser')
    },
    parse(params: IOffersParseParams): Promise<object> {
      return send('offers.Parse', params)
    },
    prepareSearchInfo(params: IOffersPrepareSearchInfoParams): Promise<object> {
      return send('offers.PrepareSearchInfo', params)
    },
    proxy(params: IOffersProxyParams): Promise<object> {
      return send('offers.Proxy', params)
    },
    publicUserOffers(params: IOffersPublicUserOffersParams): Promise<object> {
      return send('offers.PublicUserOffers', params)
    },
    reactions(params: IOffersReactionsParams): Promise<object> {
      return send('offers.Reactions', params)
    },
    rotateImage(params: IOffersRotateImageParams): Promise<string> {
      return send('offers.RotateImage', params)
    },
    searchByImage(params: IOffersSearchByImageParams): Promise<object> {
      return send('offers.SearchByImage', params)
    },
    showContacts(params: IOffersShowContactsParams): Promise<object> {
      return send('offers.ShowContacts', params)
    },
    stats(): Promise<object> {
      return send('offers.Stats')
    },
    submitForModeration(params: IOffersSubmitForModerationParams): Promise<void> {
      return send('offers.SubmitForModeration', params)
    },
    subscribeSearchTelegramUser(params: IOffersSubscribeSearchTelegramUserParams): Promise<void> {
      return send('offers.SubscribeSearchTelegramUser', params)
    },
    unPublish(params: IOffersUnPublishParams): Promise<void> {
      return send('offers.UnPublish', params)
    },
    updateDealStatus(params: IOffersUpdateDealStatusParams): Promise<void> {
      return send('offers.UpdateDealStatus', params)
    }
  },
  payments: {
    invoice(params: IPaymentsInvoiceParams): Promise<object> {
      return send('payments.Invoice', params)
    }
  },
  tickets: {
    approve(params: ITicketsApproveParams): Promise<void> {
      return send('tickets.Approve', params)
    },
    get(params: ITicketsGetParams): Promise<object> {
      return send('tickets.Get', params)
    },
    list(params: ITicketsListParams): Promise<Array<ITicket>> {
      return send('tickets.List', params)
    },
    reject(params: ITicketsRejectParams): Promise<void> {
      return send('tickets.Reject', params)
    }
  },
  users: {
    addWebSubscription(params: IUsersAddWebSubscriptionParams): Promise<void> {
      return send('users.AddWebSubscription', params)
    },
    balance(): Promise<number> {
      return send('users.Balance')
    },
    deleteUserProfile(): Promise<void> {
      return send('users.DeleteUserProfile')
    },
    formView(): Promise<object> {
      return send('users.FormView')
    },
    me(): Promise<object> {
      return send('users.Me')
    },
    profile(params: IUsersProfileParams): Promise<object> {
      return send('users.Profile', params)
    },
    promote(params: IUsersPromoteParams): Promise<void> {
      return send('users.Promote', params)
    },
    requestModerationRights(params: IUsersRequestModerationRightsParams): Promise<object> {
      return send('users.RequestModerationRights', params)
    },
    search(params: IUsersSearchParams): Promise<Array<IUser>> {
      return send('users.Search', params)
    },
    update(params: IUsersUpdateParams): Promise<void> {
      return send('users.Update', params)
    },
    updateProfile(params: IUsersUpdateProfileParams): Promise<void> {
      return send('users.UpdateProfile', params)
    },
    updateProfileData(params: IUsersUpdateProfileDataParams): Promise<void> {
      return send('users.UpdateProfileData', params)
    },
    userByID(params: IUsersUserByIDParams): Promise<object> {
      return send('users.UserByID', params)
    }
  }
});