export class YandexMapLoadError extends Error {
    sentryFingerprint: any[]
    sentryCategory = 'yandexMap'

    name = 'YandexMapLoadError'

    constructor (message: string) {
      super()

      this.message = message

      this.sentryFingerprint = [message]
    }
}

export class YandexMapWasNotMountedError extends Error {
    sentryCategory = 'yandexMap'

    name = 'YandexMapWasNotMountedError'
    message = 'Yandex map was not mounted'
}

export class YandexMapAlreadyMountedError extends Error {
    sentryCategory = 'yandexMap'

    name = 'YandexMapAlreadyMountedError'
    message = 'Yandex map already mounted'
}
