import { YandexMapLoadError } from './errors'

import { ICoordinates, ICoordinateBounds } from '~/utils/coordinates'

let loadPromise: Promise<void>|null = null

/**
 * Load the api sdk of yandex maps
 *
 * https://tech.yandex.ru/maps/jsapi/doc/2.1/quick-start/index-docpage/
 */
export async function loadYandexMaps ({ apiKey, mode = 'release' }) {
  if (loadPromise) return loadPromise

  const modules = [
    'Map',
    'Circle',
    'Balloon',
    'Placemark',
    'ObjectManager',
    'shape.Rectangle',
    'coordSystem.geo',
    'templateLayoutFactory',
    'geometry.pixel.Rectangle',
    'vectorEngine.preload'
  ].join(',')

  const path = `https://api-maps.yandex.ru/2.1/?apikey=${apiKey}&lang=ru_RU&load=${modules}&mode=${mode}`

  loadPromise = new Promise((resolve, reject) => {
    const script = document.createElement('script')
    script.setAttribute('src', path)

    document.head.appendChild(script)

    script.addEventListener('load', () => {
      // @ts-ignore
      window.ymaps.ready(resolve, (err: Error) => {
        reject(new YandexMapLoadError(err.message))
      })
    })

    script.addEventListener('error', () => {
      reject(new YandexMapLoadError('Failed to load yandex map'))
    })
  })

  return loadPromise
}

/**
 * Generate url to yandex map with a given route
 */
export function getMapRouteUrl ({ source = '', dest = '' }) {
  return `https://yandex.ru/maps/?rtext=${source}~${dest}`
}

/**
 * Generate a static map image
 *
 * https://tech.yandex.ru/maps/staticapi/doc/1.x/dg/concepts/input_params-docpage/
 */
export function getStaticMapImage (params: object) {
  const query = Object.entries(params)
    .map(([key, value]) => `${key}=${value}`)
    .join('&')

  return `https://static-maps.yandex.ru/1.x/?${query}`
}

export interface IGeocodeResult extends ICoordinates {
  kind: string
  bounds: ICoordinateBounds
}
