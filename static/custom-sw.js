
self.addEventListener('push', function (event) {
  if (event.data) {
    const data = event.data.json()
    const title = data.title || 'Новое уведомление!'
    const body = data.body || 'Что-то новое в уютном гнёздышке'

    try {
      const notification = self.registration.showNotification(title, {
        body,
        tag: 'main'
      })

      event.waitUntil(notification)
    } catch (err) {
      console.error(`error send notofocation: ${err}`)
    }
  } else {
    console.log('Push event but no data')
  }
})

self.addEventListener('notificationclick', function (event) {
  console.log('[Service Worker] Notification click Received.')

  event.notification.close()

  event.waitUntil(clients.openWindow('https://ugnest.com/'))
})
