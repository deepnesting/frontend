analyze:
	yarn build
	
build-docker:
	docker build -t ugnest_build .

start-docker:
	docker run -it -p 3000:3000 ugnest_build