<template lang="pug">
div.myMap
 button(@click="add") add
</template>

<script lang="ts">
import Vue from 'vue'
import { Component } from 'vue-class-component'
import keyBy from 'lodash/keyBy'

import { loadYandexMaps } from '~/services/yandex-maps'

@Component<Play>({
  watch: {
    items: {
      handler (newItems, _) {
        const markersDict = keyBy(newItems, 'id')
        const markerIdsToRemove: string[] = [];

        // eslint-disable-next-line no-unused-expressions
        // eslint-disable-next-line no-unexpected-multiline
        (this.objectManager?.objects as any).each((o) => {
          if (!markersDict[o.id]) {
            markerIdsToRemove.push(o.id)
          }
        })

        const toAdd = newItems?.map((x) => {
          return {
            type: 'Feature',
            id: x.id,
            geometry: {
              type: 'Point',
              coordinates: [x.lat, x.lon]
            },
            properties: {
              iconContent: x.count
            }
          }
        })
        if (this.objectManager !== null) {
          (this.objectManager as any).remove(markerIdsToRemove)
          if (toAdd) {
            this.objectManager.add(toAdd)
          }
        }
      },
      deep: true
    }
  }
})
export default class Play extends Vue {
    map: ymaps.Map|null = null;
    objectManager: ymaps.ObjectManager|null = null;
    idx = 1;
    items = [];

    async mounted () {
      await loadYandexMaps({ apiKey: '' })
      this.map = new ymaps.Map(this.$el as HTMLElement, {
        zoom: 11,
        controls: [],
        center: [59.95, 30.2]
      }, { vector: true } as any)

      // eslint-disable-next-line @typescript-eslint/no-this-alias
      const self = this

      this.map.events.add('boundschange', async (event: ymaps.IEvent) => {
        const center = event.get('newCenter')
        const zoom = event.get('newZoom')
        const bounds = event.get('newBounds')

        console.log('bounds change', center, zoom, bounds)

        try {
          const { result } = await self.$api('offers.GeoSearch', {
            search: {
              center,
              bounds,
              zoom
            }
          })

          this.items = result
        } catch (err) {
          console.error(err)
        }
      })

      this.objectManager = new ymaps.ObjectManager({ clusterize: false })
      this.objectManager.objects.events.add('click', (event: ymaps.IEvent) => {
        var objectId = event.get('objectId')
        console.log('clicked point', event, objectId)
      })

      this.objectManager.add({
        type: 'Feature',
        id: 0,
        geometry: {
          type: 'Point',
          coordinates: [59.831903, 30.411961]
        }
      })

      this.objectManager.objects.options.set('preset', 'islands#blueCircleIcon')
      this.map.geoObjects.add(this.objectManager)
    }
}
</script>

<style lang="scss" scoped>
.myMap {
  width: 100%;
  height: 600px;
}
</style>
