import Vue from 'vue'

declare module '*.vue' {
  export default Vue
}

declare module 'vue/types/vue' {
  interface Vue {
    $api: any;
    $dateFns: any;
  }
}

declare global {
  /* eslint-disable @typescript-eslint/no-use-before-define */
  const ymaps: typeof ymaps

  interface Window {
  }
}
